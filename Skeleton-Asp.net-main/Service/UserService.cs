﻿using AutoMapper;
using Contracts;
using Entities.Exceptions;
using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Shared.DataTransferObjects;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Transactions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using static Shared.Constants.Permissions;

namespace Service
{
    internal sealed class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public UserService(UserManager<User> userManager, ILoggerManager logger, IMapper mapper)
        {
            _userManager = userManager;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<(IEnumerable<UserDto> users, MetaData metaData)> GetAllUsersAsync(UserParameters userParameters, bool trackChanges)
        {
            var usersEntity = await _userManager.Users.Include(u => u.UserRoles)
                .ThenInclude(ur => ur.Role)
                .OrderBy(e => e.UserName)
                .Skip((userParameters.PageNumber - 1) * userParameters.PageSize)
                .Take(userParameters.PageSize)
                //.AsNoTracking()
                .ToListAsync();

            var count = _userManager.Users.Count();
            var usersPage = new PagedList<User>(usersEntity, count, userParameters.PageNumber, userParameters.PageSize);
            var users = new List<User>();
            foreach (User user in usersPage)
            {
                var thisUser = new User();
                thisUser.Id = user.Id;
                thisUser.UserName = user.UserName;
                thisUser.Email = user.Email;
                thisUser.Roles = new List<string>();
                foreach (var userRole in user.UserRoles)
                {
                    thisUser.Roles.Add(userRole.Role.Name);
                }
                users.Add(thisUser);
            }
            //Console.WriteLine(JsonSerializer.Serialize(users));
            var usersDto = _mapper.Map<IEnumerable<UserDto>>(users);
            return (users: usersDto, usersPage.MetaData);
            //        return companiesDto;
        }

        private async Task<List<string>> GetUserRoles(User user)
        {
            return new List<string>(await _userManager.GetRolesAsync(user));
        }

        public async Task<UserDto> GetUserAsync(string userId, bool trackChanges)
        {
            var user = await _userManager.Users.Where(c => c.Id.Equals(userId)).SingleOrDefaultAsync();
            if (user == null)
                throw new UserNotFoundException(userId);
            var userDto = _mapper.Map<UserDto>(user);
            return userDto;
        }

        public async Task<UserResultDto<IdentityResult>> CreateUserAsync(UserForCreationDto userCreationDto)
        {
            var user = _mapper.Map<User>(userCreationDto);

            var result = await _userManager.CreateAsync(user,
               userCreationDto.Password
               );
            if (result.Succeeded)
            {
                await _userManager.AddToRolesAsync(user, userCreationDto.Roles);
            }
            else
            {
                _logger.LogError($"{nameof(CreateUserAsync)}: Failed to create user, ${result.Errors}.");
            }

            var userDto = _mapper.Map<UserDto>(user);
            var userResultDto = new UserResultDto<IdentityResult>
            {
                Result = result,
                User = userDto
            };
            return userResultDto;
        }

        public async Task<UserResultDto<IdentityResult>> UpdateUserAsync(string userId, UserForUpdateDto userUpdateDto, bool trackChanges)
        {
            var user = await _userManager.FindByIdAsync(userId);
            user.UserName = userUpdateDto.UserName;
            user.Email = userUpdateDto.Email;
            if (user == null)
                throw new UserNotFoundException(userId);
            //var user = _mapper.Map<User>(userUpdateDto);

            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                string token = await _userManager.GeneratePasswordResetTokenAsync(user);
                await _userManager.ResetPasswordAsync(user, token, userUpdateDto.Password);
                await _userManager.RemoveFromRolesAsync(user, await GetUserRoles(user));
                await _userManager.AddToRolesAsync(user, userUpdateDto.Roles);
                user.Roles = userUpdateDto.Roles;
            }


            var userDto = _mapper.Map<UserDto>(user);
            var userResultDto = new UserResultDto<IdentityResult>
            {
                Result = result,
                User = userDto
            };
            return userResultDto;

        }

        public async Task<UserDto> DeleteUserAsync(string userId, bool trackChanges)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new UserNotFoundException(userId);
            await _userManager.DeleteAsync(user);
            var userDto = _mapper.Map<UserDto>(user);
            return userDto;

        }

        // public async Task<(IEnumerable<UserDto> users, string ids)> CreateUserCollectionAsync
        //(IEnumerable<UserForCreationDto> userCollection)
        // {
        //     if (userCollection is null)
        //         throw new UserCollectionBadRequest();

        //     var userEntities = _mapper.Map<IEnumerable<User>>(userCollection);
        //     foreach (var user in userEntities)
        //     {
        //         foreach (var userDto in userCollection)
        //         {
        //             _userManager.CreateAsync(user, userDto.Password);
        //         }

        //     }

        //     //await _userManager.;

        //     var userCollectionToDto = _mapper.Map<IEnumerable<UserDto>>(userEntities);
        //     var ids = string.Join(",", userCollectionToDto.Select(c => c.Id));

        //     return (users: userCollectionToDto, ids: ids);
        // }


    }
}
