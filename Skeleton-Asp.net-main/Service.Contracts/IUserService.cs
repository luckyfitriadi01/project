﻿using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Shared.DataTransferObjects;
using Shared.RequestFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Contracts
{
    public interface IUserService
    {
        Task<(IEnumerable<UserDto> users, MetaData metaData)> GetAllUsersAsync(UserParameters userParameters, bool trackChanges);
        Task<UserDto> GetUserAsync(string userId, bool trackChanges);
        Task<UserResultDto<IdentityResult>> CreateUserAsync(UserForCreationDto userDto);
        //Task<IEnumerable<UserDto>> GetByIdsAsync(IEnumerable<string> ids, bool trackChanges);
        //Task<(IEnumerable<UserDto> users, string ids)> CreateUserCollectionAsync
        //    (IEnumerable<UserForCreationDto> userCollection);
        Task<UserResultDto<IdentityResult>> UpdateUserAsync(string userid, UserForUpdateDto userForUpdate, bool trackChanges);
        Task<UserDto> DeleteUserAsync(string userId, bool trackChanges);
    }
}
