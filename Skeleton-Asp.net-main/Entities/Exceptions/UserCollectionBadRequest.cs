﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Exceptions
{
    public class UserCollectionBadRequest : BadRequestException
    {
        public UserCollectionBadRequest()
       : base("Company collection sent from a client is null.")
        {
        }
    }
}
