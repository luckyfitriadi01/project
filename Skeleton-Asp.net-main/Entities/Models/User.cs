﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class User : IdentityUser
    {
        //public string? FirstName { get; set; }
        //public string? LastName { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
        [NotMapped]
        public ICollection<string>? Roles { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }

        //public string Roles { get; set; }

    }
}
