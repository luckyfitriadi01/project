﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Role : IdentityRole
    {
        public Role(string Name)
        : base(Name) { }
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
