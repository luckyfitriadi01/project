﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DataTransferObjects
{
    public class UserRoleDto
    {
        public UserDto User { get; set; }
        public RoleDto Role { get; set; }
    }
}
