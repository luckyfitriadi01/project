﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DataTransferObjects
{
    public record UserResultDto<T>
    {
        public T Result { get; set; }
        public UserDto User { get; set; }
    }
}
