﻿using AutoMapper;
using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Shared.DataTransferObjects;

namespace SkeletonApi;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        //CreateMap<Company, CompanyDto>()
        //    .ForMember(c => c.FullAddress,
        //        opt => opt.MapFrom(x => string.Join(' ', x.Address, x.Country)));

        //CreateMap<Employee, EmployeeDto>();

        //CreateMap<CompanyForCreationDto, Company>();

        //CreateMap<EmployeeForCreationDto, Employee>();

        //CreateMap<EmployeeForUpdateDto, Employee>().ReverseMap();

        //CreateMap<CompanyForUpdateDto, Company>();
        CreateMap<User, UserDto>();
        CreateMap<UserDto, User>();
        //.ForMember(c => c.UserRoles,
        //opt => opt.MapFrom(x => x.UserRoles));
        //CreateMap<UserResultDto, IdentityResult, UserDto>
        CreateMap<UserForCreationDto, User>();
        CreateMap<UserForUpdateDto, User>();
        CreateMap<UserForRegistrationDto, User>();
    }
}
