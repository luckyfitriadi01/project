﻿using Entities.Models;
using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.Contracts;
using Shared.Constants;
using Shared.DataTransferObjects;
using Shared.RequestFeatures;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/users")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    public class UsersController : ControllerBase
    {
        private readonly IServiceManager _service;
        public UsersController(IServiceManager service) => _service = service;

        [HttpGet(Name = "GetUsers")]
        [Authorize(Policy = "Permissions.Users.View")]
        public async Task<IActionResult> GetUsers([FromQuery] UserParameters userParameters)
        {
            var pagedResult = await _service.UserService.GetAllUsersAsync(userParameters, trackChanges: false);
            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(pagedResult.metaData));
            return Ok(pagedResult.users);
        }

        [HttpGet("{id:Guid}", Name = "UserById")]
        //[ResponseCache(Duration = 60)]
        //[HttpCacheExpiration(CacheLocation = CacheLocation.Public, MaxAge = 60)]
        //[HttpCacheValidation(MustRevalidate = false)]
        [Authorize(Permissions.Users.View)]
        public async Task<IActionResult> GetUser(string id)
        {
            var user = await _service.UserService.GetUserAsync(id, trackChanges: false);
            return Ok(user);
        }

        [HttpPost(Name = "CreateUser")]
        [Authorize(Permissions.Users.Create)]
        public async Task<IActionResult> PostUser(UserForCreationDto userCreationDto)
        {
            var user = await _service.UserService.CreateUserAsync(userCreationDto);
            return Ok(user);
        }

        [HttpPut("{id:Guid}", Name = "UpdateUser")]
        [Authorize(Permissions.Users.View)]
        public async Task<IActionResult> UpdateUser(string id, UserForUpdateDto userUpdateDto)
        {
            var user = await _service.UserService.UpdateUserAsync(id, userUpdateDto, trackChanges: false);
            return Ok(user);
        }

        [HttpDelete("{id:Guid}", Name = "DeleteUser")]
        [Authorize(Permissions.Users.Delete)]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await _service.UserService.DeleteUserAsync(id, trackChanges: false);
            return Ok(user);
        }

        [HttpOptions]
        public IActionResult GetUsersOptions()
        {
            Response.Headers.Add("Allow", "GET, OPTIONS, POST, PUT, DELETE");

            return Ok();
        }

    }
}
