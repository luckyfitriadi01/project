﻿using Entities.Exceptions;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Shared.Constants;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace SkeletonApi.Presentation.Helpers.Permission
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        public PermissionAuthorizationHandler() { }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.User.Identity != null && !context.User.Identity.IsAuthenticated || context.User.Identity == null)
            {
                context.Fail();
                //context.Succeed(requirement);
                return;
            }
            if (requirement.Permission == null)
            {
                context.Succeed(requirement);
                return;
            }
            var permissionss = context.User.Claims.Where(x => x.Type == "Permission" &&
                                                                x.Value == requirement.Permission &&
                                                                  x.Issuer == "SkeletonAPI");

            if (permissionss.Any())
            {
                context.Succeed(requirement);
                return;
            }

        }

    }
}